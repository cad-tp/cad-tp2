#!/bin/bash

#only worth to run more bodies than cuda cores
#input files for the run
INPUTS=(nbod-files/nbod_init_4096.txt)

#algorithm execution versions
ALGORITHM_VERSIONS=(basic reduced)

#number of steps for each test
N_STEPS=(250) #500 2500

#io output frequency
STEPS=(5 25 125)

#number of threads to test
N_THRS=(4)

#mode of execution
MODE=evaluation

#parallel
SEQ_VERSIONS=(serial)

SMP_VERSIONS=(pthread mp)
GPU_VERSIONS=(cuda)

CUDA_KERNEL_VERSIONS=(global shmem)

BIN=./nbody_solver

DEFAULT_STEP=100

function run {
	echo "running $BIN $1 $2 $3 $4 $5 $6 $7 $8";
	$BIN $1 $2 $3 $4 $5 $6 $7 $8
}



for INPUT in "${INPUTS[@]}"
do
	for ALGORITHM_VERSION in "${ALGORITHM_VERSIONS[@]}"
	do
		for N_STEP in "${N_STEPS[@]}"
		do
			for SEQ_VERSION in "${SEQ_VERSIONS[@]}"
			do
				run $INPUT $N_STEP $MODE $DEFAULT_STEP 1 $SEQ_VERSION $ALGORITHM_VERSION global
			done
			for SMP_VERSION in "${SMP_VERSIONS[@]}"
			do
				for N_THR in "${N_THRS[@]}"
				do
					run $INPUT $N_STEP $MODE $DEFAULT_STEP $N_THR $SMP_VERSION $ALGORITHM_VERSION global
				done
			done

			for GPU_VERSION in "${GPU_VERSIONS[@]}"
			do
				for KERNEL_VERSION in "${CUDA_KERNEL_VERSIONS[@]}"
				do
					for STEP in "${STEPS[@]}"
					do
						run $INPUT $N_STEP $MODE $STEP 1 $GPU_VERSION $ALGORITHM_VERSION $KERNEL_VERSION
					done
				done
			done
		done
	done
done