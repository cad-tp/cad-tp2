#include "pth_parallel_solver.h"

pth_parallel_solver::pth_parallel_solver(long n_thr):
    parallel_solver(n_thr)
{
    thread_handles = (pthread_t*) malloc(n_thr*sizeof(pthread_t));
    thread_args = (thr_args_t*) malloc(n_thr*sizeof(thr_args_t));
    barrier_init();
    for(long i = 0; i < n_thr; i++) {
        thread_args[i].obj = this;
        thread_args[i].rank = i;
    }
}

void pth_parallel_solver::loop_schedule(long my_rank, long thread_count, long n, long sched,
    long* first_p, long* last_p, long* incr_p) {
    if (sched == PTH_CYCLIC) {
        *first_p = my_rank;
        *last_p = n;
        *incr_p = thread_count;
    } else {  /* sched == PTH_BLOCK */
        long quotient = n/thread_count;
        long remainder = n % thread_count;
        long my_iters;
        *incr_p = 1;
        if (my_rank < remainder) {
            my_iters = quotient + 1;
            *first_p = my_rank*my_iters;
        } else {
            my_iters = quotient;
            *first_p = my_rank*my_iters + remainder;
        }
        *last_p = *first_p + my_iters;
    }
}

void pth_parallel_solver::barrier_init(void) {
    b_thread_count = 0;
    pthread_mutex_init(&b_mutex, NULL);
    pthread_cond_init(&b_cond_var, NULL);
}

void pth_parallel_solver::barrier(void) {
    pthread_mutex_lock(&b_mutex);
    b_thread_count++;
    if (b_thread_count == n_thr) {
        b_thread_count = 0;
        pthread_cond_broadcast(&b_cond_var);
    } else {
        // Wait unlocks mutex and puts thread to sleep.
        //    Put wait in while loop in case some other
        // event awakens thread.
        while (pthread_cond_wait(&b_cond_var, &b_mutex) != 0);
        // Mutex is relocked at this point.
    }
    pthread_mutex_unlock(&b_mutex);
}

void pth_parallel_solver::barrier_destroy(void) {
    pthread_mutex_destroy(&b_mutex);
    pthread_cond_destroy(&b_cond_var);
}

pth_parallel_solver::~pth_parallel_solver()
{
    free(thread_handles);
    free(thread_args);
    barrier_destroy();
}
