#include "gpu_nbody_solver.h"

extern long threads_per_block;

gpu_nbody_solver::gpu_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr, mem_mode mmode):
    n_body(n,n_steps,output_freq,delta_t,init,anim), parallel_solver(n_thr), mmode(mmode)
{
	this->gpu = new gpu_info();
    allocate_device_data();
    set_device_data();
}

gpu_nbody_solver::~gpu_nbody_solver()
{
	delete gpu;
    deallocate_device_data();
}
