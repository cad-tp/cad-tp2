#include "smp_reduced_nbody_solver.h"

smp_reduced_nbody_solver::smp_reduced_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim):
    smp_nbody_solver(n,n_steps,output_freq,delta_t,init,anim)
{
    //ctor
}

smp_reduced_nbody_solver::~smp_reduced_nbody_solver()
{
    //dtor
}
