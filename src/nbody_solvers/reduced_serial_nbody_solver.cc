#include "reduced_serial_nbody_solver.h"

reduced_serial_nbody_solver::reduced_serial_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim):
    smp_reduced_nbody_solver(n,n_steps,output_freq,delta_t,init,anim)
{
    //ctor
}

void reduced_serial_nbody_solver::run() {
#   ifndef NO_OUTPUT
    output_state();
#   endif
    long part;
    for (long step = 1; step <= n_steps; step++) {
        time = step*delta_t;
        memset(forces, 0, n*sizeof(float_vector));
        for (part = 0; part < n; part++)
            compute_forces(part, forces);
        for (part = 0; part < n; part++)
            update_part(part);
#     ifndef NO_OUTPUT
        if (step % output_freq == 0 && step != n_steps)
            output_state();
#     endif
   }

#   ifndef NO_OUTPUT
    //always shows the last state
    output_state();
#   endif
}

reduced_serial_nbody_solver::~reduced_serial_nbody_solver()
{
    //dtor
}
