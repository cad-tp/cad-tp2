#include "reduced_omp_nbody_solver.h"

reduced_omp_nbody_solver::reduced_omp_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr):
    smp_reduced_nbody_solver(n,n_steps,output_freq,delta_t,init,anim), parallel_solver(n_thr)
{
    loc_forces = (float_vector*) malloc(n_thr*n*sizeof(float_vector));
}

void reduced_omp_nbody_solver::run() {
#   ifndef NO_OUTPUT
    output_state();
#   endif
    long step;
    long part;
    float_vector* forces = this->forces;
    float_vector* loc_forces = this->loc_forces;
    long n_thr = this->n_thr;
    float delta_t = this->delta_t;
    long n = this->n;
    long n_steps = this->n_steps;
    long output_freq = this->output_freq;
    float time = this->time;
#   pragma omp parallel num_threads(n_thr) default(none) \
        shared(forces,n_thr,delta_t,n,n_steps, \
                output_freq,loc_forces) \
        private(step, part, time)
    {
        long my_rank = omp_get_thread_num();
        long thread;

        for (step = 1; step <= n_steps; step++) {
            time = step*delta_t;
//          memset(loc_forces + my_rank*n, 0, n*sizeof(float_vector));
#           pragma omp for
            for (part = 0; part < n_thr*n; part++)
                loc_forces[part][X] = loc_forces[part][Y] = 0.0;
#           ifdef DEBUG
#           pragma omp single
            {
                __yal_log("Step %ld, after memset loc_forces = \n", step);
                for (part = 0; part < n_thr*n; part++)
                    __yal_log("%ld %e %e\n", part, loc_forces[part][X],
                        loc_forces[part][Y]);
                __yal_log("\n");
            }
#           endif
            /* Particle n-1 will have all forces computed after call to
            * Compute_force(n-2, . . .) */
#           pragma omp for schedule(dynamic, 1)
            for (part = 0; part < n-1; part++)
                compute_forces(part, loc_forces + my_rank*n);
#           pragma omp for schedule(dynamic, 1)
            for (part = 0; part < n; part++) {
                forces[part][X] = forces[part][Y] = 0.0;
                for (thread = 0; thread < n_thr; thread++) {
                    forces[part][X] += loc_forces[thread*n + part][X];
                    forces[part][Y] += loc_forces[thread*n + part][Y];
                }
            }
#           pragma omp for schedule(dynamic, 1)
            for (part = 0; part < n; part++)
                update_part(part);
#           ifndef NO_OUTPUT
            if (step % output_freq == 0 && step != n_steps) {
#               pragma omp single
                output_state();
            }
#           endif
        }  /* for step */
    }  /* pragma omp parallel */

#   ifndef NO_OUTPUT
    //always shows the last state
    output_state();
#   endif
    this->time = n_steps*delta_t;
}

reduced_omp_nbody_solver::~reduced_omp_nbody_solver()
{
    free(loc_forces);
}
