#include "gpu_reduced_nbody_solver.h"

extern long threads_per_block;

gpu_reduced_nbody_solver::gpu_reduced_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr, mem_mode mmode):
    gpu_nbody_solver(n,n_steps,output_freq,delta_t,init,anim,n_thr,mmode)
{
    handle(cudaMalloc(&d_f, n * sizeof(float_vector)));
}

void gpu_reduced_nbody_solver::run() {
#   ifndef NO_OUTPUT
    output_state();
#   endif
    for (long step = 1; step <= n_steps; step++) {
        time = step*delta_t;
        run_reduced_compute_forces(n,d_p,d_f,delta_t, mmode, gpu->get_threads_per_block());
        if (step != n_steps) {
            this->get_device_data();
#           ifndef NO_OUTPUT
            output_state();
#           endif
        }
    }

    time = n_steps*delta_t;
    //to update ram values to the new ones computed
    this->get_device_data();
#   ifndef NO_OUTPUT
    output_state();
#   endif
    wait_device();
}

gpu_reduced_nbody_solver::~gpu_reduced_nbody_solver()
{
    handle(cudaFree(d_f));
}


