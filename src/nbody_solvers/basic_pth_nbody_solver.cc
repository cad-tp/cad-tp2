#include "basic_pth_nbody_solver.h"

basic_pth_nbody_solver::basic_pth_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr):
    smp_basic_nbody_solver(n,n_steps,output_freq,delta_t,init,anim), pth_parallel_solver(n_thr)
{

}

void* basic_pth_nbody_solver::work(void* arg) {
    long my_rank = ((thr_args_t *) arg)->rank;
    long step;    /* Current step      */
    long part;    /* Current particle  */
    long first;   /* My first particle */
    long last;    /* My last particle  */
    long incr;    /* Loop increment    */

    loop_schedule(my_rank, n_thr, n, PTH_BLOCK, &first, &last, &incr);
    for (step = 1; step <= n_steps; step++) {
        time = step*delta_t;
        /* Particle n-1 will have all forces computed after call to
        * Compute_force(n-2, . . .) */
        for (part = first; part < last; part += incr)
            compute_forces(part, forces);
        barrier();
        for (part = first; part < last; part += incr)
            update_part(part);
        barrier();
#       ifndef NO_OUTPUT
        if (step % output_freq == 0 && my_rank == 0 && step != n_steps) {
            output_state();
        }
#       endif
    }  /* for step */

#       ifndef NO_OUTPUT
        if (my_rank == 0) {
            output_state();
        }
#       endif
    return NULL;
}

static void* pth_work(void* data) {
    thr_args* arg = (thr_args *) data;
    return arg->obj->work(arg);
}


void basic_pth_nbody_solver::run() {
#   ifndef NO_OUTPUT
    output_state();
#   endif
    long thread;
    for (thread = 0; thread < n_thr; thread++)
        pthread_create(&thread_handles[thread], NULL, pth_work, (void*) &thread_args[thread]);

    for (thread = 0; thread < n_thr; thread++)
        pthread_join(thread_handles[thread],NULL);
}


basic_pth_nbody_solver::~basic_pth_nbody_solver()
{

}
