#include "nbody.h"
#include "configuration.h"
#include "gpu_info.h"

__global__ void global_reduced_kernel(long n, particle* particles, float_vector* forces, float delta_t) {
    long part = blockDim.x * blockIdx.x + threadIdx.x;

    if(part < n) {
        forces[part][X] = 0.0;
        forces[part][Y] = 0.0;
    }

    __syncthreads();

    if(part < n) {
       long k;
        float mg;
        float_vector f_part_k;
        float len, len_3, fact;
        for (k = part+1; k < n; k++) {
            f_part_k[X] = particles[part].pos[X] - particles[k].pos[X];
            f_part_k[Y] = particles[part].pos[Y] - particles[k].pos[Y];
            len = sqrt(f_part_k[X]*f_part_k[X] + f_part_k[Y]*f_part_k[Y]);
            len_3 = len*len*len + D_EPS*D_EPS*D_EPS;
            mg = D_G * particles[part].mass * particles[k].mass;
            fact = mg/len_3;
            f_part_k[X] *= fact;
            f_part_k[Y] *= fact;
            forces[part][X] += f_part_k[X];
            forces[part][Y] += f_part_k[Y];
            forces[k][X] -= f_part_k[X];
            forces[k][Y] -= f_part_k[Y];
        }
    }

    __syncthreads();

    if(part < n) {
        float fact = delta_t/particles[part].mass;

        particles[part].pos[X] += delta_t * particles[part].velocity[X];
        particles[part].pos[Y] += delta_t * particles[part].velocity[Y];
        particles[part].velocity[X] += fact * forces[part][X];
        particles[part].velocity[Y] += fact * forces[part][Y];
    }
}

__global__ void shmem_reduced_kernel(long n, particle* particles, float_vector* forces, float delta_t) {
    long part = blockDim.x * blockIdx.x + threadIdx.x;
    long loc_part = threadIdx.x;
    
    __shared__ particle l_parts[CUDA_MAX_THREADS_PER_BLOCK];
    if(part < n) {
        forces[part][X] = 0;
        forces[part][Y] = 0;
    }

    __syncthreads();

    if(part < n) {
        l_parts[loc_part] = particles[part];
        long k;
        float mg;
        float_vector f_part_k;
        float len, len_3, fact;
        for (k = part+1; k < n; k++) {
            f_part_k[X] = l_parts[loc_part].pos[X] - l_parts[loc_part].pos[X];
            f_part_k[Y] = l_parts[loc_part].pos[Y] - l_parts[loc_part].pos[Y];
            len = sqrt(f_part_k[X]*f_part_k[X] + f_part_k[Y]*f_part_k[Y]);
            len_3 = len*len*len + D_EPS*D_EPS*D_EPS;
            mg = D_G * l_parts[loc_part].mass * particles[k].mass;
            fact = mg/len_3;
            f_part_k[X] *= fact;
            f_part_k[Y] *= fact;
            forces[part][X] += f_part_k[X];
            forces[part][Y] += f_part_k[Y];
            forces[k][X] -= f_part_k[X];
            forces[k][Y] -= f_part_k[Y];
        }
    }

    __syncthreads();

    if(part < n) {
        float fact = delta_t/l_parts[loc_part].mass;

        particles[part].pos[X] = l_parts[loc_part].pos[X] + delta_t * l_parts[loc_part].velocity[X];
        particles[part].pos[Y] = l_parts[loc_part].pos[Y] + delta_t * l_parts[loc_part].velocity[Y];
        particles[part].velocity[X] = l_parts[loc_part].velocity[X] + fact * forces[part][X];
        particles[part].velocity[Y] = l_parts[loc_part].velocity[Y] + fact * forces[part][Y];
    }
}

void run_reduced_compute_forces(long n, particle* d_p, float_vector* d_f, float delta_t, mem_mode mmode, long threads_per_block) {
    long threadsPerBlock = threads_per_block;
    long blocksPerGrid   = ceil(float(n)/float(threadsPerBlock));

    if(mmode == mem_mode::global) {
        global_reduced_kernel<<<blocksPerGrid,threadsPerBlock>>>(n, d_p, d_f, delta_t);
    } else {
        shmem_reduced_kernel<<<blocksPerGrid,threadsPerBlock>>>(n, d_p, d_f, delta_t);
    }
}
