#include <stdio.h>

#include "nbody.h"
#include "configuration.h"
#include "gpu_info.h"

__global__ void global_basic_kernel(long n, particle* particles, float delta_t) {
    long part = blockDim.x * blockIdx.x + threadIdx.x;
    float_vector force;
    if(part < n) {
        long k;
        float mg;
        float_vector f_part_k;
        float len, len_3, fact;
        for (k = 0; k < n; k++) {
            if(k == part) continue;
            f_part_k[X] = particles[part].pos[X] - particles[k].pos[X];
            f_part_k[Y] = particles[part].pos[Y] - particles[k].pos[Y];
            len = sqrt(f_part_k[X]*f_part_k[X] + f_part_k[Y]*f_part_k[Y]);
            len_3 = len*len*len + D_EPS*D_EPS*D_EPS;
            mg = D_G * particles[part].mass*particles[k].mass;
            fact = mg/len_3;
            f_part_k[X] *= fact;
            f_part_k[Y] *= fact;
            force[X] += f_part_k[X];
            force[Y] += f_part_k[Y];
        }
    }
    __syncthreads();
    if(part < n) {
        float fact = delta_t/particles[part].mass;
        particles[part].pos[X] += delta_t * particles[part].velocity[X];
        particles[part].pos[Y] += delta_t * particles[part].velocity[Y];
        particles[part].velocity[X] += fact * force[X];
        particles[part].velocity[Y] += fact * force[Y];
    }
    __syncthreads();
}

__global__ void shmem_basic_kernel(long n, particle* particles, float delta_t) {
    long part = blockDim.x * blockIdx.x + threadIdx.x;
    long loc_part = threadIdx.x;
    __shared__ particle l_parts[CUDA_MAX_THREADS_PER_BLOCK];
    float_vector force;
    if(part < n) {
        l_parts[loc_part] = particles[part];
        long k;
        float mg;
        float_vector f_part_k;
        float len, len_3, fact;
        for (k = 0; k < n; k++) {
            if(k == part) continue;
            particle k_p = particles[k];
            f_part_k[X] = l_parts[loc_part].pos[X] - k_p.pos[X];
            f_part_k[Y] = l_parts[loc_part].pos[Y] - k_p.pos[Y];
            len = sqrt(f_part_k[X]*f_part_k[X] + f_part_k[Y]*f_part_k[Y]);
            len_3 = len*len*len + D_EPS*D_EPS*D_EPS;
            mg = D_G * l_parts[loc_part].mass*k_p.mass;
            fact = mg/len_3;
            f_part_k[X] *= fact;
            f_part_k[Y] *= fact;
            force[X] += f_part_k[X];
            force[Y] += f_part_k[Y];
        }
    }
    __syncthreads();
    if(part < n) {
        float fact = delta_t/particles[part].mass;

        particles[part].pos[X] = l_parts[loc_part].pos[X] + delta_t * l_parts[loc_part].velocity[X];
        particles[part].pos[Y] = l_parts[loc_part].pos[Y] + delta_t * l_parts[loc_part].velocity[Y];
        particles[part].velocity[X] = l_parts[loc_part].velocity[X] + fact * force[X];
        particles[part].velocity[Y] = l_parts[loc_part].velocity[Y] + fact * force[Y];
    }
    __syncthreads();
}

void run_basic_compute_forces(long n, particle* d_p, float delta_t, mem_mode mmode, long threads_per_block) {
    long n_thr = threads_per_block;
    long n_block   = ceil(float(n)/float(n_thr));

    if(mmode == mem_mode::global) {
        global_basic_kernel<<<n_block,n_thr>>>(n, d_p, delta_t);
    } else {
        shmem_basic_kernel<<<n_block,n_thr>>>(n, d_p, delta_t);
    }
}
