#include "reduced_pth_nbody_solver.h"

reduced_pth_nbody_solver::reduced_pth_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr):
    smp_reduced_nbody_solver(n,n_steps,output_freq,delta_t,init,anim),
    pth_parallel_solver(n_thr)
{
    loc_forces = (float_vector*) malloc(n_thr*n*sizeof(float_vector));
}

void* reduced_pth_nbody_solver::work(void* arg) {
    long my_rank = ((thr_args_t *) arg)->rank;
    long thread;
    long step;     /* Current step                   */
    long part;     /* Current particle               */
    long bfirst;   /* My first particle in blk sched */
    long blast;    /* My last particle in blk sched  */
    long bincr;    /* Loop increment in blk sched    */
    long cfirst;   /* My first particle in cyc sched */
    long clast;    /* My last particle in cyc sched  */
    long cincr;    /* Loop increment in cyc sched    */

    loop_schedule(my_rank, n_thr, n, PTH_BLOCK, &bfirst, &blast, &bincr);
    loop_schedule(my_rank, n_thr, n, PTH_CYCLIC, &cfirst, &clast, &cincr);
    for (step = 1; step <= n_steps; step++) {
        time = step*delta_t;
        /* Particle n-1 will have all forces computed after call to
        * Compute_force(n-2, . . .) */
        memset(loc_forces + my_rank*n, 0, n*sizeof(float_vector));
        /* Barrier isn't needed:  Next loop will only work with
        * my part of loc_forces */
        barrier();
        for (part = cfirst; part < clast; part += cincr)
            compute_forces(part, loc_forces + my_rank*n);
        barrier();
        for (part = bfirst; part < blast; part += bincr) {
            forces[part][X] = forces[part][Y] = 0.0;
            for (thread = 0; thread < n_thr; thread++) {
                forces[part][X] += loc_forces[thread*n + part][X];
                forces[part][Y] += loc_forces[thread*n + part][Y];
            }
        }
        barrier();
        for (part = bfirst; part < blast; part += bincr)
            update_part(part);
        barrier();
#       ifndef NO_OUTPUT
        if (step % output_freq == 0 && my_rank == 0 && step != n_steps) {
            output_state();
        }
#       endif
    }
#       ifndef NO_OUTPUT
        if (my_rank == 0) {
            output_state();
        }
#       endif
    return NULL;
}

static void* pth_work(void* data) {
    thr_args* arg = (thr_args *) data;
    return arg->obj->work(arg);
}


void reduced_pth_nbody_solver::run() {
#   ifndef NO_OUTPUT
    output_state();
#   endif
    long thread;
    for (thread = 0; thread < n_thr; thread++)
        pthread_create(&thread_handles[thread], NULL, pth_work, (void*) &thread_args[thread]);

    for (thread = 0; thread < n_thr; thread++)
        pthread_join(thread_handles[thread], NULL);
}

reduced_pth_nbody_solver::~reduced_pth_nbody_solver()
{

}
