#include <iostream>
#include <fstream>
#include <string.h>
#include <assert.h>

#ifdef __APPLE__
    #define get_nprocs_conf() 8
#else
    #include <sys/sysinfo.h>
#endif

#include "logger.h"
#include "particle.h"
#include "nbody.h"

#include "statistics.h"
#include "time_lib.h"

#include "basic_serial_nbody_solver.h"
#include "reduced_serial_nbody_solver.h"

#include "basic_pth_nbody_solver.h"
#include "reduced_pth_nbody_solver.h"

#include "basic_omp_nbody_solver.h"
#include "reduced_omp_nbody_solver.h"

#include "gpu_basic_nbody_solver.h"
#include "gpu_reduced_nbody_solver.h"

using namespace std;

void usage(char* prog_name) {
   cerr << "Usage " << prog_name << " <inputFile> <iterationSteps> <[animation|evaluation|check]> <arguments>" << std::endl;
   cerr << "   for animation args  - <nSteps>" << std::endl;
   cerr << "   for evaluation args - <nSteps> <nThreads> <[serial|pthread|mp|cuda]> <[basic|reduced]> <[global|shmem]>" << std::endl;
   cerr << "   for check args - <nSteps> <nThreads> <[serial|pthread|mp|cuda]> <[basic|reduced]> <[global|shmem]> <output filename>" << std::endl;
   exit(0);
}

exec_mode get_args(int argc, char* argv[], ifstream& str, long& iterationSteps) {
    if(argc != 5 && argc != 9 && argc != 10) {
        __yal_log("Wrong number of arguments %d\n", argc);
        usage(argv[0]);
    }
    __yal_log("Input filename: %s\n", argv[1]);
    str.open(argv[1]);
    iterationSteps = atol(argv[2]);
    __yal_log("Number of iteration steps: %ld\n",iterationSteps);

    if (strcmp(argv[3], ANIMATION) == 0) { // READING ARGS FOR ANIMATION MODE
        __yal_log("Execution Mode: ANIMATION\n");
        return anim;
    } else if(strcmp(argv[3], EVALUATION) == 0) { // READING ARGS FOR EVALUATION MODE
        __yal_log("Execution Mode: EVALUATION\n");
        return eval;
    } else if(strcmp(argv[3], CHECK) == 0) {
        __yal_log("Execution Mode: CHECK\n");
        return check;
    } else { // WRONG MODE
        usage(argv[0]);
        return anim;
    }
}

long parse_particles(ifstream& reader, particle** partics) {
    long num_parts;
    reader >> num_parts;
    *partics = (particle *) malloc(num_parts * sizeof(particle));
    float mass;
    float_vector pos, vel;
    for(long i = 0; i < num_parts; i++) {
        reader >> mass >> pos[0] >> pos[1] >> vel[0] >> vel[1];
        mass = mass * (1.0 + WEIGHT_FACTOR);
        pos[0] = pos[0] * (1.0 + POS_FACTOR);
        pos[1] = pos[1] * (1.0 + POS_FACTOR);
        vel[0] = vel[0] * (1.0 + VEL_FACTOR);
        vel[1] = vel[1] * (1.0 + VEL_FACTOR);
        (*partics)[i] = particle(mass, pos, vel);
    }
    return num_parts;
}

void print_particles(particle*& partics, long num_partics) {
    for(long i = 0; i < num_partics; i++) {
        cout << partics[i].mass << " " << partics[i].pos[0] << " " << partics[i].pos[1] << " " << partics[i].velocity[0] << " " << partics[i].velocity[1] << endl;
    }
}

n_body* get_solver(int argc, char** argv, exec_mode emode, long n_steps, particle* partics, long num_partics, float delta_t, char** name) {
    long output_freq = atol(argv[4]);
    long n_thr = get_nprocs_conf();
    solve_mode smode = basic;
    parallel_mode pmode = cuda;
    mem_mode mmode = shared;
    switch(emode) {
        case anim:
            return new reduced_omp_nbody_solver(num_partics, n_steps, output_freq, delta_t, partics, emode==exec_mode::anim, n_thr);
            break;
        case check:
            *name = argv[9];
        case eval:
            n_thr = atol(argv[5]);
            if (strcmp(argv[6], SERIAL_NB_ARG) == 0) {
                __yal_log("Solve Mode: SERIAL_NB_ARG\n");
                pmode = serial;
            } else if(strcmp(argv[6], PTHREAD_NB_ARG) == 0) {
                __yal_log("Solve Mode: PTHREAD_NB_ARG\n");
                pmode = pth;
            } else if(strcmp(argv[6], OMP_NB_ARG) == 0) {
                __yal_log("Solve Mode: OMP_NB_ARG\n");
                pmode = mp;
            } else if(strcmp(argv[6], CUDA_NB_ARG) == 0) {
                __yal_log("Solve Mode: CUDA_NB_ARG\n");
                pmode = cuda;
            } else { // WRONG MODE
                usage(argv[0]);
            }

            if (strcmp(argv[7], NB_BASIC) == 0) {
                __yal_log("Solve Mode: NB_BASIC\n");
                smode = basic;
            } else if(strcmp(argv[7], NB_REDUCED) == 0) {
                __yal_log("Solve Mode: NB_REDUCED\n");
                smode = reduced;
            } else { // WRONG MODE
                usage(argv[0]);
            }

            if (strcmp(argv[8], CUDA_USE_GLOBAL) == 0) {
                __yal_log("Solve Mode: CUDA_USE_GLOBAL\n");
                mmode = global;
            } else if(strcmp(argv[8], CUDA_USE_SHARED) == 0) {
                __yal_log("Solve Mode: CUDA_USE_SHARED\n");
                mmode = shared;
            } else { // WRONG MODE
                usage(argv[0]);
            }
            break;
    }

    switch(pmode) {
    case serial:
        switch(smode) {
        case basic:
            //place here the serial basic version of the nbody solver
            return new basic_serial_nbody_solver(num_partics,n_steps,output_freq,delta_t,partics,emode==exec_mode::anim);
            break;
        case reduced:
            //place here the serial reduced version of the nbody solver
            return new reduced_serial_nbody_solver(num_partics,n_steps,output_freq,delta_t,partics,emode==exec_mode::anim);
            break;
        }
        break;
    case pth:
        switch(smode) {
        case basic:
            //place here the pthread basic version of the nbody solver
            return new basic_pth_nbody_solver(num_partics, n_steps, output_freq, delta_t, partics, emode==exec_mode::anim, n_thr);
            break;
        case reduced:
            //place here the pthread reduced version of the nbody solver
            return new reduced_pth_nbody_solver(num_partics, n_steps, output_freq, delta_t, partics, emode==exec_mode::anim, n_thr);
            break;
        }
        break;
    case mp:
        switch(smode) {
        case basic:
            //place here the mp basic version of the nbody solver
            return new basic_omp_nbody_solver(num_partics, n_steps, output_freq, delta_t, partics, emode==exec_mode::anim, n_thr);
            break;
        case reduced:
            //place here the mp reduced version of the nbody solver
            return new reduced_omp_nbody_solver(num_partics, n_steps, output_freq, delta_t, partics, emode==exec_mode::anim, n_thr);
            break;
        }
        break;
    case cuda:
        switch(smode) {
        case basic:
            //place here the cuda basic version of the nbody solver
            return new gpu_basic_nbody_solver(num_partics, n_steps, output_freq, delta_t, partics, emode==exec_mode::anim, n_thr,mmode);
            break;
        case reduced:
            //place here the cuda reduced version of the nbody solver
            return new gpu_reduced_nbody_solver(num_partics, n_steps, output_freq, delta_t, partics, emode==exec_mode::anim, n_thr,mmode);
            break;
        }
        break;
    }
    return NULL;
}

int main(int argc, char** argv)
{
    ifstream reader;
    long it_steps;

    exec_mode mode = get_args(argc, argv, reader, it_steps);

    particle* partics;
    long num_partics = parse_particles(reader, &partics);
    reader.close();

    char* name;
    
    n_body* nb = get_solver(argc, argv, mode, it_steps, partics, num_partics,TIME_STEP,&name);
    
    assert(nb);

    if(mode == anim) {
        char* tile_name = const_cast<char*>(WINDOW_NAME);
        nb->setupX(tile_name, argc, argv);
        nb->run();
    } else if (mode == check) {
        nb->run();
        nb->write_state(name);
    } else {
        statistics stats(NUM_TESTS,DISCARD);
        int i;
        for(i = 0; i < NUM_TESTS; i++) {
            __yal_log("starting benchmark %d\n", i+1);

            time_lib chr;
            __yal_log("main:Starting test (%d)\n", i);
            chr.start();
            nb->run();
            chr.finish();
            __yal_log("main:Ending test (%d)\n", i);

            stats.add_value(chr.get_time());
            nb->reset();
        }

        printf("Average time:\t\t%lf seconds\nStandard Deviation:\t%lf seconds\n",
            stats.get_avg(), stats.get_std_dev());
    }
    return 0;
}
