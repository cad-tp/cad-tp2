#include "configuration.h"

#include <stdlib.h>

#ifndef PARALLEL_SOLVER_H
#define PARALLEL_SOLVER_H


class parallel_solver
{
public:
    parallel_solver(long n_thr = 1);

    virtual inline long get_nthr() {
        return this->n_thr;
    }

    virtual void run() = 0;

    virtual ~parallel_solver();
protected:
    long n_thr;
};

#endif // PARALLEL_SOLVER_H
