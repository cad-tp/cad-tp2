#include "smp_nbody_solver.h"

#ifndef SMP_BASIC_NBODY_SOLVER_H
#define SMP_BASIC_NBODY_SOLVER_H


class smp_basic_nbody_solver: public smp_nbody_solver
{
public:
    smp_basic_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim);

    virtual void run() = 0;

    virtual void compute_forces(long part, float_vector* forces) {
        long k;
        float mg;
        float_vector f_part_k;
        float len, len_3, fact;

        __yal_log("Current total force on particle %ld = (%.3e, %.3e)\n",
                part, forces[part][X], forces[part][Y]);
        forces[part][X] = forces[part][Y] = 0.0;

        for (k = 0; k < n; k++) {
            if(k == part) continue;
            f_part_k[X] = particles[part].pos[X] - particles[k].pos[X];
            f_part_k[Y] = particles[part].pos[Y] - particles[k].pos[Y];
            len = sqrt(f_part_k[X]*f_part_k[X] + f_part_k[Y]*f_part_k[Y]);
            len_3 = len*len*len + EPS*EPS*EPS;
            mg = G*particles[part].mass*particles[k].mass;
            fact = mg/len_3;
            f_part_k[X] *= fact;
            f_part_k[Y] *= fact;
            __yal_log("Force on particle %ld due to particle %ld = (%.3e, %.3e)\n",
               part, k, f_part_k[X], f_part_k[Y]);
   
            /* Add force in to total forces */
            forces[part][X] += f_part_k[X];
            forces[part][Y] += f_part_k[Y];
        }
    }

    virtual ~smp_basic_nbody_solver();
};

#endif // SMP_BASIC_NBODY_SOLVER_H
