#include "nbody.h"

#ifndef SMP_NBODY_SOLVER_H
#define SMP_NBODY_SOLVER_H


class smp_nbody_solver: public n_body
{
public:
    smp_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim);

    virtual void update_part(long part);

    virtual ~smp_nbody_solver();
protected:
	float_vector* forces;
};

#endif // SMP_NBODY_SOLVER_H
