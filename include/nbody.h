#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#include <X11/Xlib.h> // X11 library headers
#include <X11/Xutil.h>
#include <X11/Xos.h>

#include "particle.h"
#include "configuration.h"
#include "utils/logger.h"

#ifndef NBODY_H
#define NBODY_H


class n_body
{
public:

    //pre condition: init particles must have been allocated
    n_body(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim):
        n(n), n_steps(n_steps), output_freq(output_freq), delta_t(delta_t), particles(init), animate(anim)
    {
        __yal_log("n_body::n_body: construct: anim (%d)\n",anim);
        this->particles = NULL;
        this->init = init;
        this->time = 0.0;
        this->reset();
    }

    virtual void setupX(char* god_name, int argc, char** argv) {
        this->setupAnimation(god_name, argc, argv);
    }

    virtual void reset() {
        if(this->particles != NULL) {
            free(this->particles);
        }
        this->particles = copy_particles();
    }

    virtual void run() = 0;

    virtual void show_animation() {
        if(!animate) return;
        long color;
        int x, y;

        XClearWindow(mydisplay,mywindow); // clear window for next drawing
        color=BODY_COLOR;
        XSetForeground(mydisplay,mygc,(long)color);

        
        for(long i = 0; i < n; i++) {
            x = particles[i].pos[X] / SCREEN_POS_FACTOR;
            y = particles[i].pos[Y] / SCREEN_POS_FACTOR;

            x += MONITOR_WIDTH/2;
            y += MONITOR_HEIGHT/2;
            XDrawPoint(mydisplay, mywindow, mygc, x, y);
        }
        
        XFlush(mydisplay); // necessary to write to display
        usleep(SLEEP_TIME);
    }

    virtual void output_state() {
        this->show_animation();
        long part;
        __yal_log("%.2lf\n", time);
        for(part = 0; part < n; part++) {
            __yal_log("%.3e ", particles[part].mass);
            __yal_log("%3ld %10.3e ", part, particles[part].pos[X]);
            __yal_log("  %10.3e ", particles[part].pos[Y]);
            __yal_log("  %10.3e ", particles[part].velocity[X]);
            __yal_log("  %10.3e\n", particles[part].velocity[Y]);
        }
        __yal_log("\n");
    }

    virtual void write_state(char* name) {
        FILE* out = fopen(name,"w");
        fprintf(out, "%.2lf\n", time);
        for(long part = 0; part < n; part++) {
            fprintf(out, "%.3e ", particles[part].mass);
            fprintf(out, "%3ld %10.3e ", part, particles[part].pos[X]);
            fprintf(out, "  %10.3e ", particles[part].pos[Y]);
            fprintf(out, "  %10.3e ", particles[part].velocity[X]);
            fprintf(out, "  %10.3e\n", particles[part].velocity[Y]);
        }
        fprintf(out, "\n");
        fclose(out);
    }

    virtual ~n_body() {
        if(particles != NULL)
            free(particles);
        free(init);
        if(anim) {
            this->closeAnimation();
        }
    };

protected:

    particle* copy_particles() {
        particle* parts = (particle*) malloc(n * sizeof(particle));
        for (long i = 0; i < n; i++) {
            parts[i] = init[i];
        }
        return parts;
    }

    long n;                  /* Number of particles              */
    long n_steps;            /* Number of timesteps              */
    long output_freq;        /* Frequency of output              */
    float delta_t;          /* Size of timestep                 */
    float time;                /* Current time                     */
    particle* init;          /* Initial state of the particles   */
    particle* particles;     /* Current state of the particles   */
    bool animate;            /* If an animation must be made     */

private:
    void setupAnimation(char *title, int argc, char *argv[]) {
        XSizeHints myhint;
        XGCValues values;
        XSetWindowAttributes attr[1];
        int display_width, display_height;
        unsigned long myforeground, mybackground;

        if ( (mydisplay = XOpenDisplay (NULL)) == NULL ) {
            /* connect to Xserver */
            fprintf(stderr, "Cannot connect to X server %s\n",XDisplayName (NULL) );
            exit(-1);
        }

        myscreen = DefaultScreen(mydisplay); // get screen size
        display_width= DisplayWidth (mydisplay, myscreen);
        display_height= DisplayHeight (mydisplay, myscreen);
        printf("display_width = %d; display_height = %d \n",
            display_width, display_height);

        //mybackground = WhitePixel(mydisplay,myscreen);
        mybackground = BlackPixel(mydisplay,myscreen);
        myforeground = BlackPixel(mydisplay,myscreen);

        /* Suggest where to position the window: */
        myhint.x = 50;
        myhint.y = 50;
        myhint.width = MONITOR_WIDTH;
        myhint.height = MONITOR_HEIGHT;
        myhint.flags = PPosition | PSize;
        myhint.min_width= 300;
        myhint.min_height= 300;

        /* Create a window - not displayed yet however: */
        mywindow = XCreateSimpleWindow(mydisplay,
                        DefaultRootWindow(mydisplay),
                        myhint.x,myhint.y,myhint.width,myhint.height,
                        5,myforeground,mybackground);
        XSetStandardProperties(mydisplay,mywindow,title,title,None,
			 argv,argc,&myhint);
        XSetNormalHints(mydisplay, mywindow, &myhint);
        XStoreName(mydisplay, mywindow, title);

        /* Create a Graphics Context (GC) for the window: */
        mygc = XCreateGC(mydisplay,mywindow,0,&values);
        XSetBackground(mydisplay,mygc,mybackground);
        XSetForeground(mydisplay,mygc,myforeground);

        XSetLineAttributes(mydisplay, mygc, 1, LineSolid, CapRound, JoinRound);
        attr[0].backing_store= Always;
        attr[0].backing_planes= 1;
        attr[0].backing_pixel= BlackPixel(mydisplay, myscreen);
        XChangeWindowAttributes(mydisplay,mywindow,
                CWBackingStore | CWBackingPlanes | CWBackingPixel, attr);

        /* Select input devices to listen to: */
        XSelectInput(mydisplay,mywindow,ButtonPressMask|KeyPressMask|ExposureMask);

        /* Actually display the window: */
        XMapRaised(mydisplay,mywindow);

        /* Main Event Loop: This is the core of any X program: */

        XNextEvent(mydisplay,&myevent);
        switch(myevent.type) {
            case Expose: /* Repaint window on expose */
            if (myevent.xexpose.count==0)
            break;
        }
    }

    void closeAnimation() {
        XFreeGC(mydisplay,mygc);
        XDestroyWindow(mydisplay,mywindow);
        XCloseDisplay(mydisplay);
    }

    Display *mydisplay;
    Window mywindow;
    GC mygc;
    XEvent myevent;
    KeySym mykey;
    int myscreen;
};

#endif // NBODY_H
