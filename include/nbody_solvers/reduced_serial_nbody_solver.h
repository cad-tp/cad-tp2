#include "smp_reduced_nbody_solver.h"

#ifndef REDUCED_SERIAL_NBODY_SOLVER_H
#define REDUCED_SERIAL_NBODY_SOLVER_H


class reduced_serial_nbody_solver: public smp_reduced_nbody_solver
{
public:
    reduced_serial_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim);
    virtual void run();
    virtual ~reduced_serial_nbody_solver();
};

#endif // REDUCED_SERIAL_NBODY_SOLVER_H
