#include "smp_reduced_nbody_solver.h"
#include "pth_parallel_solver.h"

#include <omp.h>

#ifndef REDUCED_PTH_NBODY_SOLVER_H
#define REDUCED_PTH_NBODY_SOLVER_H


class reduced_pth_nbody_solver : public smp_reduced_nbody_solver, public pth_parallel_solver
{
public:
    reduced_pth_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr);
    virtual void run();
    virtual void* work(void* rank);
    virtual ~reduced_pth_nbody_solver();
protected:
    float_vector* loc_forces;
};

#endif // REDUCED_PTH_NBODY_SOLVER_H
