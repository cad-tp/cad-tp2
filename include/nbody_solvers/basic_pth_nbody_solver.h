#include "smp_basic_nbody_solver.h"
#include "pth_parallel_solver.h"

#ifndef BASIC_PTH_NBODY_SOLVER_H
#define BASIC_PTH_NBODY_SOLVER_H

class basic_pth_nbody_solver : public smp_basic_nbody_solver, public pth_parallel_solver {
public:
    basic_pth_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr);
    virtual void run();
    void* work(void* rank);
    virtual ~basic_pth_nbody_solver();
};

#endif // BASIC_PTH_NBODY_SOLVER_H
