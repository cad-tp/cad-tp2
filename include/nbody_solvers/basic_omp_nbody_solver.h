#include "smp_basic_nbody_solver.h"
#include "parallel_solver.h"

#include <omp.h>

#ifndef BASIC_OMP_NBODY_SOLVER_H
#define BASIC_OMP_NBODY_SOLVER_H


class basic_omp_nbody_solver: public smp_basic_nbody_solver, public parallel_solver
{
public:
    basic_omp_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr);
    virtual void run();
    virtual ~basic_omp_nbody_solver();
};

#endif // BASIC_OMP_NBODY_SOLVER_H
