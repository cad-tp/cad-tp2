#include "gpu_nbody_solver.h"
#include "kernel_header.h"

#ifndef GPU_REDUCED_NBODY_SOLVER_H
#define GPU_REDUCED_NBODY_SOLVER_H


class gpu_reduced_nbody_solver: public gpu_nbody_solver
{
public:
    gpu_reduced_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr, mem_mode mmode);
    virtual void run();
    virtual ~gpu_reduced_nbody_solver();

protected:
    float_vector* d_f;
};

#endif // GPU_REDUCED_NBODY_SOLVER_H
